#include "SDK.h"
#include "Panels.h"
#include "Client.h"
#include "CDrawManager.h"

CScreenSize gScreenSize;
//===================================================================================
void __fastcall Hooked_PaintTraverse( PVOID pPanels, int edx, unsigned int vguiPanel, bool forceRepaint, bool allowForce )
{
	try
	{
		VMTManager& hook = VMTManager::GetHook(pPanels); //Get a pointer to the instance of your VMTManager with the function GetHook.
		hook.GetMethod<void(__thiscall*)(PVOID, unsigned int, bool, bool)>(gOffsets.iPaintTraverseOffset)(pPanels, vguiPanel, forceRepaint, allowForce); //Call the original.

		static unsigned int vguiMatSystemTopPanel;

		if (vguiMatSystemTopPanel == NULL)
		{
			const char* szName = gInts.Panels->GetName(vguiPanel);
			if( szName[0] == 'M' && szName[3] == 'S' ) //Look for MatSystemTopPanel without using slow operations like strcmp or strstr.
			{
				vguiMatSystemTopPanel = vguiPanel;
				Intro();
			}
		}
		
		if ( vguiMatSystemTopPanel == vguiPanel ) //If we're on MatSystemTopPanel, call our drawing code.
		{
			if (gInts.Engine->IsDrawingLoadingImage() || !gInts.Engine->IsInGame() || !gInts.Engine->IsConnected() || gInts.Engine->Con_IsVisible() || ((GetAsyncKeyState(VK_F12) || gInts.Engine->IsTakingScreenshot())))
			{
				return; //We don't want to draw at the menu.
			}

			//gDrawManager.DrawString((gScreenSize.iScreenWidth / 2) - 55, 200, gDrawManager.dwGetTeamColor(3), "Welcome to Darkstorm"); //Remove this if you want.

			if (GetAsyncKeyState(VK_F10) & 0x8000)
			{
				try
				{
					Vector vScreen, vWorldPos;
					for (int iIndex = 0; iIndex <= gInts.EntList->GetHighestEntityIndex(); iIndex++)
					{
						CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

						if (pBaseEntity == NULL)
							continue;

						pBaseEntity->GetWorldSpaceCenter(vWorldPos);
						if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
							continue;

						if (pBaseEntity->GetClientClass()->iClassID == 2 || pBaseEntity->GetClientClass()->iClassID == 89)
						{
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFFFFFFF, "%s", gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()));
						}
						else
						{
							gDrawManager.DrawString(vScreen.x, vScreen.y, 0xFFFFFFFF, "%s %i", pBaseEntity->GetClientClass()->chName, pBaseEntity->GetClientClass()->iClassID);
						}
						static bool bLogEnts;
						if (!bLogEnts)
						{
							gBaseAPI.LogToFile("Begin logging Classes");
							for (ClientClass* pCC = gInts.Client->GetAllClasses(); pCC; pCC = pCC->pNextClass)
							{
								gBaseAPI.LogToFile("Class: %s Recv: %s ID: %i", pCC->chName, pCC->Table->GetName(), pCC->iClassID);
							}
							gBaseAPI.LogToFile("End of logging Classes");
							bLogEnts = true;
						}
					}
				}
				catch (...)
				{
					gBaseAPI.LogToFile("Failed DrawESP");
				}
			}
			else
			{
				Vector vScreen, vWorldPos;
				for (int iIndex = 0; iIndex <= gInts.EntList->GetHighestEntityIndex(); iIndex++)
				{
					if (iIndex == me)
						continue;

					CBaseEntity* pBaseEntity = GetBaseEntity(iIndex);

					if (pBaseEntity == NULL)
						continue;

					pBaseEntity->GetWorldSpaceCenter(vWorldPos);
					if (!gDrawManager.WorldToScreen(vWorldPos, vScreen))
						continue;

					int iID = pBaseEntity->GetClientClass()->iClassID;

					if (iID == 2)
					{
						if (strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "button") || strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "switch"))
						{
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0xFF0000FF, "Button");
						} 
					}

					if (iID == 108)
					{
						gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x00FFFFFF, pBaseEntity->GetPlayerName());
						continue;
					}

					if (iID == 88) //Personality Core
					{
						switch (pBaseEntity->GetSkin())
						{
						case 0:
							gDrawManager.DrawString(vScreen.x - 30, vScreen.y - 6, 0x0080FFFF, "Wheatly");
							break;
						case 1:
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x00FF00FF, "Rick");
							break;
						case 2:
							gDrawManager.DrawString(vScreen.x - 35, vScreen.y - 6, 0xFFED24FF, "Space Core");
							break;
						case 3:
							gDrawManager.DrawString(vScreen.x - 30, vScreen.y - 6, 0xFF7FEDFF, "Fact Core");
							break;
						}
						continue;
					}

					if (iID == 125)
					{
						//gDrawManager.DrawString( false, vScreen.x - 15, vScreen.y - 6, 0x0080FFFF, "Portal" );
						continue;
					}

					if (iID == 136)
					{
						if (strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "metal_box.mdl"))
						{
							switch (pBaseEntity->GetSkin())
							{
								case 0:
								case 2:
								case 5:
									gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x0000FFFF, "Box");
									continue;
								case 1:
									gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0xFF7FEDFF, "Companion Cube");
									continue;
								case 3:
									gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x0000FFFF, "Reflector Box");
									continue;
								case 4:
									gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x0000FFFF, "Sphere");
									continue;
							}
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x0000FFFF, "Box");
						}
						else if (strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "reflection_cube.mdl"))
						{
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x0000FFFF, "Reflector Box");
						}
						else if (strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "underground_weighted_cube.mdl"))
						{
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x0000FFFF, "Box");
						}
						continue;
					}

					if (iID == 99)
					{
						if (strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "monster_a.mdl"))
						{
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x0000FFFF, "Frankenturret");
						}
						else if (strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "monster_A_box"))
						{
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x0000FFFF, "Box");
						}
						continue;
					}

					if (iID == 87 || iID == 90 || iID == 89)
					{

						if (strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "skeleton") || strstr(gInts.ModelInfo->GetModelName(pBaseEntity->GetModel()), "backwards") || pBaseEntity->GetSkin() == 1 )
						{
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0x808080FF, "Defective Turret");
						}
						else
						{
							gDrawManager.DrawString(vScreen.x - 15, vScreen.y - 6, 0xFF8000FF, "Turret");
						}
						continue;
					}
				}
			}
		}
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed PaintTraverse");
		gBaseAPI.ErrorBox("Failed PaintTraverse");
	}
}
//===================================================================================
void Intro( void )
{
	try
	{
		gDrawManager.Initialize(); //Initalize the drawing class.

		//gBaseAPI.LogToFile("Injection Successful"); //If the module got here without crashing, it is good day.
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed Intro");
		gBaseAPI.ErrorBox("Failed Intro");
	}
}