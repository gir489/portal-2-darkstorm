#include "SDK.h"
#include "Client.h"
#include "Panels.h"

COffsets gOffsets;
CPlayerVariables gPlayerVars;
CInterfaces gInts;

CreateInterface_t EngineFactory = NULL;
CreateInterface_t ClientFactory = NULL;
CreateInterface_t VGUIFactory = NULL;
CreateInterface_t VGUI2Factory = NULL;

DWORD WINAPI dwMainThread( LPVOID lpArguments )
{
	if (gInts.Client == NULL) //Prevent repeat callings.
	{
		//Gottammove those factorys up.
		//Grab the factorys from their resptive module's EAT.
		ClientFactory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "client.dll" ), "CreateInterface" );
		gInts.Client = ( CHLClient* )ClientFactory( "VClient016", NULL);
		XASSERT(gInts.Client);
		gInts.EntList = ( CEntList* ) ClientFactory( "VClientEntityList003", NULL );
		EngineFactory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "engine.dll" ), "CreateInterface" );
		gInts.Engine = ( EngineClient* ) EngineFactory( "VEngineClient015", NULL );
		XASSERT(gInts.Engine);
		gInts.ModelInfo = (CModelInfo*)EngineFactory("VModelInfoClient004", NULL);
		XASSERT(gInts.ModelInfo);
		VGUIFactory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "vguimatsurface.dll" ), "CreateInterface" );
		gInts.Surface = ( ISurface* ) VGUIFactory( "VGUI_Surface031", NULL );
		XASSERT(gInts.Surface);
		VGUI2Factory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "vgui2.dll" ), "CreateInterface" );

		//Setup the ClientMode hooks.
		gInts.ClientMode = GetClientMode();
		VMTBaseManager* clientMode = new VMTBaseManager(); //Setup our VMTBaseManager for Panels.
		clientMode->Init(gInts.ClientMode);
		clientMode->HookMethod(&Hooked_CreateMove, gOffsets.iCreateMoveOffset); //ClientMode create move is called inside of CHLClient::CreateMove, and thus no need for hooking WriteUserCmdDelta.
		clientMode->Rehook();

		//Setup the Panel hook so we can draw.
		if( !gInts.Panels )
		{
			gInts.Panels = ( IPanel* ) VGUI2Factory( "VGUI_Panel009", NULL );
			XASSERT( gInts.Panels );

			if( gInts.Panels )
			{
				VMTBaseManager* panelHook = new VMTBaseManager(); //Setup our VMTBaseManager for Panels.
				panelHook->Init(gInts.Panels);
				panelHook->HookMethod(&Hooked_PaintTraverse, gOffsets.iPaintTraverseOffset);
				panelHook->Rehook();
			}
		}
	}
	return 0; //The thread has been completed, and we do not need to call anything once we're done. The call to Hooked_PaintTraverse is now our main thread.
}

int WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	//If you manually map, make sure you setup this function properly.
	if(dwReason == DLL_PROCESS_ATTACH)
	{
		CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)dwMainThread, NULL, 0, NULL ); //CreateThread > _BeginThread. (For what we're doing, of course.)
	}
	return true;
}