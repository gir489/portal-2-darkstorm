#include "SDK.h"
#include "Client.h"

//============================================================================================
void __fastcall Hooked_CreateMove(PVOID ClientMode, int edx, float input_sample_frametime, CUserCmd* pCommand)
{
	try
	{
		CBaseEntity* pBaseEntity = GetBaseEntity(me); //Grab the local player's entity pointer.

		if (pBaseEntity == NULL) //This should never happen, but never say never. 0xC0000005 is no laughing matter.
			return;

		VMTManager& hook = VMTManager::GetHook(gInts.ClientMode); //Get a pointer to the instance of your VMTManager with the function GetHook.
		hook.GetMethod<void(__thiscall*)(PVOID, float, CUserCmd*)>(gOffsets.iCreateMoveOffset)(gInts.ClientMode, input_sample_frametime, pCommand); //Call the original.

		//Do your client hook stuff here. This function is called once per tick. For time-critical functions, run your code in PaintTraverse. For move specific functions, run them here.

		int iFlags = *(PDWORD)(pBaseEntity + 0xF8);
		//Bunnyhop seems kinda useless, but whatever. Might as well do something with CreateMove.
		if (pCommand->buttons & IN_JUMP)
		{
			pCommand->buttons &= ~IN_JUMP;
			if (iFlags & FL_ONGROUND)
			{
				pCommand->buttons |= IN_JUMP;
			}
		}
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed Hooked_CreateMove");
	}
}
//============================================================================================
const char* CBaseEntity::GetPlayerName()
{
	//This could be done better...
	typedef IGameResources* (__stdcall* GetGameResources_t)(void);
	static GetGameResources_t GetGameResourcesCall = (GetGameResources_t)gBaseAPI.GetClientSignature("A1 ? ? ? ? 85 C0 74 06 05");

	return GetGameResourcesCall()->GetPlayerName(this->GetIndex());
}
//============================================================================================
PDWORD GetClientMode()
{
	//Because Portal 2 uses split screen, I need to call this stupid wrapper function.
	static DWORD clientModeAddress = gBaseAPI.dwGetCallFunctionLocation(gBaseAPI.GetClientSignature("F3 0F 11 80 ? ? ? ? E8 ? ? ? ? 8B 10 F3 0F 10 45") + 8);
	typedef PDWORD(__stdcall* ClientModeFn)(void);
	static ClientModeFn ClientMode = (ClientModeFn)clientModeAddress;
	return ClientMode();
}